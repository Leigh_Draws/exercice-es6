const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false },
  ];

// Créez une fonction addTask, en utilisant une fonction fléchée et l'opérateur spread.

const addTask = (task) => {
  let newTask = [...task, ...tasks] ;
  return newTask
}

const taskOne = [{title : 'Ranger le placard', isComplete: false}]

console.log(addTask(taskOne))

// Fonction fléchée qui récupère les tâches du tableau "tasks" et renvoie seulement les tâches non complétées, ne fonctionne que sur le tableau tasks
const incompleteTasks = tasks.filter((eachtask) => {
  if (eachtask.isComplete === false) {
    return true
  } else {
    return false
  }
})

console.log(incompleteTasks)

// Fonction fléchée qui prend une variable en paramètre et qui vérifie si la clé "isComplete" est vraie ou fausse et associe une nouvelle valeur

const taskTwo = {title : 'Faire la vaisselle', isComplete: false}

const toggleTaskStatus = (singleTask) => {
  if (singleTask.isComplete === false) {
    singleTask.isComplete = true
  } else if (singleTask.isComplete === true) {
    singleTask.isComplete = false
  }
  return singleTask
}

console.log(toggleTaskStatus(taskTwo))
